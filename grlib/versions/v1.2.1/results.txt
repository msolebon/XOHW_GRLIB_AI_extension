#cycles TestName MatSize (simd|orig) speedup
2.003.992 cifar10         32x32 simd X

   19.897 polynomial      512   simd 4X
   78.906 polynomial      512   orig 1X
 
   79.417 polynomial      2048  simd 4.41X
  350.265 polynomial      2048  orig 1X

   79.417 polynomial      2048  simd 15.01X
1.192.342 polynomial_sat  2048  orig 1X
