-----------------------------------------------------------------------------
--  LEON3 Xilinx KCU105 Demonstration design
------------------------------------------------------------------------------
--  This file is a part of the GRLIB VHDL IP LIBRARY
--  Copyright (C) 2003 - 2008, Gaisler Research
--  Copyright (C) 2008 - 2014, Aeroflex Gaisler
--  Copyright (C) 2015 - 2021, Cobham Gaisler
--
--  This program is free software; you can redistribute it and/or modify
--  it under the terms of the GNU General Public License as published by
--  the Free Software Foundation; either version 2 of the License, or
--  (at your option) any later version.
--
--  This program is distributed in the hope that it will be useful,
--  but WITHOUT ANY WARRANTY; without even the implied warranty of
--  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
--  GNU General Public License for more details.
--
--  You should have received a copy of the GNU General Public License
--  along with this program; if not, write to the Free Software
--  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA 
------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library grlib, techmap;
use grlib.amba.all;
use grlib.devices.all;
use grlib.stdlib.all;
use techmap.gencomp.all;
use techmap.allclkgen.all;

library gaisler;
use gaisler.memctrl.all;
use gaisler.uart.all;
use gaisler.misc.all;
use gaisler.spi.all;
use gaisler.net.all;
use gaisler.jtag.all;
use gaisler.i2c.all;
use gaisler.l2cache.all;
use gaisler.subsys.all;
use gaisler.axi.all;
use gaisler.spacewire.all;
use gaisler.leon3.all;
-- pragma translate_off
use gaisler.sim.all;

library unisim;
use unisim.all;
-- pragma translate_on

library testgrouppolito;
use testgrouppolito.dprc_pkg.all;

use work.config.all;

entity leon3mp is
  generic (
    fabtech                 : integer := CFG_FABTECH;
    memtech                 : integer := CFG_MEMTECH;
    padtech                 : integer := CFG_PADTECH;
    clktech                 : integer := CFG_CLKTECH;
    disas                   : integer := CFG_DISAS;   -- Enable disassembly to console
    dbguart                 : integer := CFG_DUART;   -- Print UART on console
    pclow                   : integer := CFG_PCLOW;
    migmodel                : boolean := false;
    autonegotiation         : integer := 1
  );
  port (
    -- Clock and Reset
    reset       : in    std_ulogic;
    clk300p     : in    std_ulogic;  -- 300 MHz clock
    clk300n     : in    std_ulogic;  -- 300 MHz clock
    -- UART
    dsurx       : in    std_ulogic;
    dsutx       : out   std_ulogic
--    dsuctsn     : in    std_ulogic;
--    dsurtsn     : out   std_ulogic
  );
end;


architecture rtl of leon3mp is

  -----------------------------------------------------
  -- Constants ----------------------------------------
  -----------------------------------------------------

  constant in_simulation : boolean := false
  --pragma translate_off
                                      or true
  --pragma translate_on
                                      ;
  constant in_synthesis : boolean := not in_simulation;
  constant maxahbm      : integer := 2;
  constant maxahbs      : integer := 4;

  -- AHB MASTERS
  constant hi_leon3     : integer := 0;
  constant hi_ahbuart   : integer := 1;

  -- AHB SLAVES
  constant hi_dsu       : integer := 0;
  constant hi_apbctrl   : integer := 1;
  constant hi_ahbram0   : integer := 2;
  constant hi_ahbrom0   : integer := 3;

  -- APB SLAVES
  constant pi_ahbuart   : integer := 0;
  constant pi_ahbstat   : integer := 1;
  constant pi_irqgen    : integer := 2;
  constant pi_apbuart   : integer := 3;
  constant pi_l3stat    : integer := 4;
  --constant pi_logan     : integer := 5;

  constant OEPOL        : integer := padoen_polarity(padtech);

  constant BOARD_FREQ   : integer := 300000; -- input frequency in KHz
  constant CPU_FREQ     : integer := BOARD_FREQ * CFG_CLKMUL / CFG_CLKDIV; -- cpu frequency in KHz

  constant ramfile      : string := "test.srec"; -- ram contents

  -----------------------------------------------------
  -- Signals ------------------------------------------
  -----------------------------------------------------

  signal irqi : irq_in_vector(0 to CFG_NCPU - 1);
  signal irqo : irq_out_vector(0 to CFG_NCPU - 1);

  signal dsui : dsu_in_type;
  signal dsuo : dsu_out_type;

  signal dbgi : l3_debug_in_vector(0 to CFG_NCPU-1);
  signal dbgo : l3_debug_out_vector(0 to CFG_NCPU-1);

  -- Misc
  signal stati          : ahbstat_in_type;
  --signal logan_signals  : std_logic_vector(95 downto 0);

  -- APB
  signal apbi           : apb_slv_in_type;
  signal apbo           : apb_slv_out_vector := (others => apb_none);

  -- AHB
  signal ahbsi          : ahb_slv_in_type;
  signal ahbso          : ahb_slv_out_vector := (others => ahbs_none);
  signal ahbmi          : ahb_mst_in_type;
  signal ahbmo          : ahb_mst_out_vector := (others => ahbm_none);
  signal mig_ahbsi      : ahb_slv_in_type;
  signal mig_ahbso      : ahb_slv_out_type;

 -- Clocks and Reset
  signal clkm           : std_ulogic := '0';
  signal lock           : std_ulogic;
  signal rstn           : std_ulogic;
  signal rstraw         : std_ulogic;
  signal cgi            : clkgen_in_type;
  signal cgo            : clkgen_out_type;


  attribute keep         : boolean;
  attribute keep of clkm : signal is true;

  -- APB UART
  signal u1i            : uart_in_type;
  signal u1o            : uart_out_type;

  -- AHB UART
  signal dui            : uart_in_type;
  signal duo            : uart_out_type;

component clk_wiz_0 is  
  port(
    clk_out1  : out std_logic;
    clk_in1_p : in  std_logic;
    clk_in1_n : in  std_logic
  );
  end component;
begin

  ----------------------------------------------------------------------
  ---  Reset and Clock generation  -------------------------------------
  ----------------------------------------------------------------------
  
  rst0 : rstgen generic map (acthigh => 1)
    port map (reset, clkm, '1', rstn, rstraw);

  -- clock generator
  clkgen0 : clk_wiz_0
    port map (clk_out1 => clkm, clk_in1_p => clk300p, clk_in1_n => clk300n);

--  cgi.pllctrl <= "00";
--  cgi.pllrst <= rstraw;
--
--  
--  rst0 : rstgen generic map (acthigh => 1)
--    port map (reset, clkm, lock, rstn, rstraw);
--  lock <= cgo.clklock;
--
--  -- clock generator
--  clkgen0 : clkgen
--    generic map (fabtech, CFG_CLKMUL, CFG_CLKDIV, 0, 0, 0, 0, 0, BOARD_FREQ, 0)
--    port map (clk300p, '0', clkm, open, open, open, open, cgi, cgo, open, open, open);


  ----------------------------------------------------------------------
  ---  LEDs and BUTTONs ------------------------------------------------
  ----------------------------------------------------------------------
    -- DISABLED 


  ----------------------------------------------------------------------
  ---  AHB & APB CONTROLLER --------------------------------------------
  ----------------------------------------------------------------------

  ahb0 : ahbctrl  -- AHB arbiter/multiplexer
    generic map (defmast => CFG_DEFMST, split => CFG_SPLIT,
                 rrobin  => CFG_RROBIN, ioaddr => CFG_AHBIO, fpnpen => CFG_FPNPEN,
                 nahbm   => maxahbm, nahbs => maxahbs)
    port map (
      rst  => rstn,
      clk  => clkm,
      msti => ahbmi,
      msto => ahbmo,  -- Incoming accesses
      slvi => ahbsi,  -- Outgoing accesses
      slvo => ahbso
    );

  apb0 : apbctrl
    generic map (hindex => hi_apbctrl, haddr => CFG_APBADDR, nslaves => 16)
    port map (rstn, clkm, ahbsi, ahbso(hi_apbctrl), apbi, apbo);

  -----------------------------------------------------------------------------
  -- AHB & APB UART -----------------------------------------------------------
  -----------------------------------------------------------------------------
  duart : ahbuart
    generic map (hindex => hi_ahbuart,  pindex => pi_ahbuart, paddr => pi_ahbuart)
    port map (rstn, clkm, dui, duo, apbi, apbo(pi_ahbuart), ahbmi, ahbmo(hi_ahbuart));

  dui.rxd    <= dsurx;      -- AHB input data
  dsutx      <= duo.txd;    -- AHB output 
 -- dui.ctsn   <= dsuctsn; 
  --dsurtsn    <= duo.rtsn;

  uart1 : apbuart
    generic map (pindex   => pi_apbuart, paddr => pi_apbuart, pirq => 2, console => dbguart,
                 fifosize => CFG_UART1_FIFO)
    port map (rstn, clkm, apbi, apbo(pi_apbuart), u1i, u1o);

  u1i.rxd    <= u1o.txd;
  u1i.ctsn   <= '0';
  u1i.extclk <= '0';

  -----------------------------------------------------------------------
  ---  AHB RAM ----------------------------------------------------------
  -----------------------------------------------------------------------
  ram: if in_synthesis = true generate -- synthesis ram
    ahbram0 : ahbram
      generic map (hindex => hi_ahbram0, haddr => 16#400#, tech => CFG_MEMTECH, kbytes => 1024, pipe => 0)
      port map (rstn, clkm, ahbsi, ahbso(hi_ahbram0));
  end generate;

    --pragma translate_off
  sim_ram: if in_simulation = true generate -- simulation ram
    ahbram0 : ahbram_sim
      generic map (hindex => hi_ahbram0, haddr => 16#400#, tech => 0, kbytes => 1024, pipe => 0, fname => ramfile)
      port map (rstn, clkm, ahbsi, ahbso(hi_ahbram0));
  end generate;
     --pragma translate_on


  -----------------------------------------------------------------------
  ---  AHB ROM ----------------------------------------------------------
  -----------------------------------------------------------------------
--  brom : entity work.ahbrom
--      generic map (hindex => hi_ahbrom0, haddr => CFG_AHBRODDR, pipe => CFG_AHBROPIP)
--      port map (rstn, clkm, ahbsi, ahbso(hi_ahbrom0));

  ----------------------------------------------------------------------
  --- INTERRUPT CONTROLLER ---------------------------------------------
  ----------------------------------------------------------------------
  irqctrl0 : irqmp                      -- interrupt controller
    generic map (pindex => pi_irqgen, paddr => pi_irqgen, ncpu => CFG_NCPU)
    port map (rstn, clkm, apbi, apbo(pi_irqgen), irqo, irqi);
  
  -----------------------------------------------------------------------
  ---  AHB Status Register ----------------------------------------------
  -----------------------------------------------------------------------
  stati <= ahbstat_in_none;
  ahbstat0 : ahbstat
    generic map (pindex => pi_ahbstat, paddr => pi_ahbstat, pirq => 7,
                 nftslv => CFG_AHBSTATN)
    port map(rstn, clkm, ahbmi, ahbsi, stati, apbi, apbo(pi_ahbstat));

  ----------------------------------------------------------------------
  ---  L3STAT ----------------------------------------------------------
  ----------------------------------------------------------------------
    l3stat0 : l3stat
    generic map (pindex => pi_l3stat, paddr => pi_l3stat , pmask => 16#FFE#, ncnt => 4, ncpu => CFG_NCPU, 
                 nmax => 1, lahben => 1, dsuen => CFG_DSU, nextev=>0)
                 --try nmax with 0, 
    port map (rstn => rstn, clk => clkm, apbi => apbi, apbo => apbo(pi_l3stat), ahbsi => ahbsi, dbgo => dbgo, dsuo => dsuo);

  ----------------------------------------------------------------------
  ---  LOGAN Logic Analyzer  -------------------------------------------
  ----------------------------------------------------------------------

--   logan0 : logan
--   generic map (dbits=>96, pindex => pi_logan, paddr => pi_logan, pmask => 16#fff#, memtech => memtech)
--   port map (rstn, clkm, clkm, apbi, apbo(pi_logan), logan_signals);

  ----------------------------------------------------------------------
  ---  LEON3 processor and DSU -----------------------------------------
  ----------------------------------------------------------------------
    leon3 : for i in 0 to CFG_NCPU-1 generate
      u0 : leon3s                       -- LEON3 processor
        generic map (
          hindex      => i,
          fabtech     => fabtech,      memtech     => memtech,
          nwindows    => CFG_NWIN,
          dsu         => CFG_DSU,
          fpu         => CFG_FPU,
          v8          => CFG_V8,
          cp          => 0,
          mac         => CFG_MAC,
          pclow       => pclow,
          notag       => 0,
          nwp         => CFG_NWP,
          icen        => CFG_ICEN,      irepl       => CFG_IREPL,      isets       => CFG_ISETS,
          ilinesize   => CFG_ILINE,     isetsize    => CFG_ISETSZ,     isetlock    => CFG_ILOCK,
          dcen        => CFG_DCEN,      drepl       => CFG_DREPL,      dsets       => CFG_DSETS,
          dlinesize   => CFG_DLINE,     dsetsize    => CFG_DSETSZ,     dsetlock    => CFG_DLOCK,
          dsnoop      => CFG_DSNOOP,
          ilram       => CFG_ILRAMEN,   ilramsize   => CFG_ILRAMSZ,    ilramstart  => CFG_ILRAMADDR,
          dlram       => CFG_DLRAMEN,   dlramsize   => CFG_DLRAMSZ,    dlramstart  => CFG_DLRAMADDR,
          mmuen       => CFG_MMUEN,
          itlbnum     => CFG_ITLBNUM,   dtlbnum     => CFG_DTLBNUM,
          tlb_type    => CFG_TLB_TYPE,  tlb_rep     => CFG_TLB_REP,
          lddel       => CFG_LDDEL,
          disas       => disas,
          tbuf        => CFG_ITBSZ,
          pwd         => CFG_PWD,
          svt         => CFG_SVT,
          rstaddr     => CFG_RSTADDR,
          smp         => CFG_NCPU-1,
          cached      => CFG_DFIXED,
          scantest    => CFG_SCAN,
          mmupgsz     => CFG_MMU_PAGE,
          bp          => 1,
          npasi       => CFG_NP_ASI,
          pwrpsr      => CFG_WRPSR)
        port map (clkm, rstn, ahbmi, ahbmo(hi_leon3), ahbsi, ahbso,
                  irqi(i), irqo(i), dbgi(i), dbgo(i));
    end generate;
 
  dsugen : if CFG_DSU = 1 generate
    dsu0 : dsu3                         -- LEON3 Debug Support Unit
      generic map (hindex => hi_dsu, haddr => 16#900#, hmask => 16#F00#,
                   ncpu   => CFG_NCPU, tbits => 30, tech => memtech, irq => 0, kbytes => CFG_ATBSZ)
      port map (rstn, clkm, ahbmi, ahbsi, ahbso(hi_dsu), dbgo, dbgi, dsui, dsuo);
    dsui.enable <= '1';
  --dsui.break <= gpioi.din(0);
  end generate;

  nodsu : if CFG_DSU = 0 generate
    dsuo.tstop <= '0'; dsuo.active <= '0'; ahbso(hi_dsu) <= ahbs_none;
  end generate;
  -----------------------------------------------------------------------
  ---  Test report module  ----------------------------------------------
  -----------------------------------------------------------------------

  -- pragma translate_off
  test0 : ahbrep
    generic map (hindex => 5, haddr => 16#200#)
    port map (rstn, clkm, ahbsi, ahbso(5));
  -- pragma translate_on

  -----------------------------------------------------------------------
  ---  Boot message  ----------------------------------------------------
  -----------------------------------------------------------------------

  -- pragma translate_off
  x : report_design
    generic map (
      msg1    => "LEON3/GRLIB Xilinx ZCU102 Demonstration design",
      fabtech => tech_table(fabtech), memtech => tech_table(memtech),
      mdel    => 1
    );
-- pragma translate_on

 end;
