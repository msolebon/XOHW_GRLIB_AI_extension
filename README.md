# Xilinx Open Hardware - Hardware-Software Co-design for Low-cost AI processing in Space Processors

## Project description 
This project started as part of my master course Processor Design, later it was extended as my master thesis. With this project I compete in the Xilinx Open Hardware competition in the student category. For the project I have modified the LEON3 space processor and have included a SIMD module with features targeting acceleration in artificial intelligence applications. 

The SIMD module, operates over the integer unit registers (SIMD within a register, aka SWAR) and has two stages. In the first stage both input operands are operated against each other at byte granularity, the result is passed to the second stage where reduction operations are implemented. Additionally a mask vector can restrict the bytes to be computed in the first stage and swizzling option is included. 

The SPARC cross-compiler is also included with support for SIMD instructions in assembly.

**IMPORTANT:** Read the README located at bcc-2.2.0 directory for how to build it and add some additional required files.

A previous version of this work was presented in the OBDP 2021 workshop organized by the European Space Agency. The paper presented in the talk is included in the repository.
## File organization
In the current repository only a subset of the provided designs in the GRLIB are included, the recomended to use for simulation is the **leon3-minimal** (GRLIB/designs/leon3-minimal). 

Different tests are included in the software directory, currently only for the leon3, in GRLIB/software/marcmod/leon3. In the design folder a mymake script is included which can be used to generate the test file for a program by executing: *./mymake $testname [-s testsize] [-p <in|out|all>]*. Testname ending with \_simd make use of the custom SIMD module. 

The SIMD module and all future additions is found under grlib/libs/marcmod, although some modifications, identified in the file with *marcmod* are also found in grlib/libs/gaisler/leon3v3, specially the iu3.vhd file.

A working design for a Zynq Ultrascale+ FPGA can be found in grlib/design/leon3-xilinx-zcu102. To start the project execute *make vivado-launch*.


## Licence
The work is released under GPL license according to original files which can be found in: https://www.gaisler.com/index.php/downloads/leongrlib
